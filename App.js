/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';

import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
// import TextRecognition from 'react-native-text-recognition';
import TesseractOcr, {
  LANG_ENGLISH,
  LEVEL_WORD,
} from 'react-native-tesseract-ocr';
import RNTextDetector from 'rn-text-detector';

const App = () => {
  const API_KEY = 'AIzaSyDHKML4hYtVPth-HWVQZexeeOC2uIOMLJo';
  const [image, setImage] = useState();
  const [detectedText, setDetectedText] = useState();
  const [showLoader, setShowloader] = useState(false);

  const onTakePhoto = () =>
    launchCamera({mediaType: 'image', includeBase64: true}, onImageSelect);

  const onSelectImagePress = () =>
    launchImageLibrary(
      {mediaType: 'image', includeBase64: true},
      onImageSelect,
    );

  const onImageSelect = async media => {
    if (!media.didCancel) {
      console.log(media);
      setImage(media.uri);
      // const processingResult = await TextRecognition.recognize(
      //   media.assets[0].uri,
      //   {
      //     visionIgnoreThreshold: 0.5,
      //   },
      // );

      // const tessOptions = {level: LEVEL_WORD};
      // const processingResult = TesseractOcr.recognizeTokens(
      //   media.assets[0].uri,
      //   LANG_ENGLISH,
      //   tessOptions,
      // );

      try {
        // const tesseractOptions = {};
        // const recognizedText = await TesseractOcr.recognize(
        //   media.assets[0].uri,
        //   LANG_ENGLISH,
        //   tesseractOptions,
        // );
        // console.log(processingResult);
        const visionResp = await RNTextDetector.detectFromUri(
          media.assets[0].uri,
        );

        // console.log('visionResp::::::::::::', visionResp);
        // setDetectedText(visionResp[8].text);

        detectText(media.assets[0].base64);
      } catch (err) {
        console.log(err);
        // setText('');
      }
    }
  };

  const detectText = base64 => {
    setShowloader(true);
    fetch('https://vision.googleapis.com/v1/images:annotate?key=' + API_KEY, {
      method: 'POST',
      body: JSON.stringify({
        requests: [
          {
            image: {content: base64},
            features: [{type: 'TEXT_DETECTION'}],
          },
        ],
      }),
    })
      .then(response => {
        return response.json();
      })
      .then(jsonRes => {
        let text = jsonRes.responses[0].fullTextAnnotation.text;
        const lines = text.match(/[^\r\n]+/g);
        setDetectedText(text);
        console.log('Text Detected ::::', text);

        console.log('Lines Detected ::::', lines);
        setShowloader(false);
        // setDetectedText(text);
      })
      .catch(err => {
        console.log('Error', err);
      });
  };

  return (
    <ScrollView contentContainerStyle={styles.screen}>
      <Text style={styles.title}>Text Recognition</Text>
      <View>
        <TouchableOpacity style={styles.button} onPress={onTakePhoto}>
          <Text style={styles.buttonText}>Take Photo</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={onSelectImagePress}>
          <Text style={styles.buttonText}>Pick a Photo</Text>
        </TouchableOpacity>
        {showLoader ? (
          <ActivityIndicator size="large" style={styles.loader} />
        ) : (
          <ScrollView style={{marginBottom: 300}}>
            <Text style={{marginVertical: 20}}>{detectedText}</Text>
          </ScrollView>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    // alignItems: 'center',
    marginHorizontal: 20,
  },
  title: {
    fontSize: 35,
    marginVertical: 40,
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#47477b',
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 40,
    borderRadius: 50,
    marginTop: 20,
  },
  buttonText: {
    color: '#fff',
  },
  image: {
    height: 300,
    width: 300,
    marginTop: 30,
    borderRadius: 10,
  },
  loader: {
    // position: 'absolute',
    // flex: 1,
    // backgroundColor: 'red',
    alignSelf: 'center',
    marginTop: 40,
  },
});

export default App;
